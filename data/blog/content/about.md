+++
title = "About"
description = "What is this site and who I am"
date = "2022-05-24"
aliases = ["about-us", "about-hugo", "contact"]
author = "Sergei Valuev"
+++

Hello! My name is Sergei Valuev. I am a DevOps engineer and system administrator in the near past.

This site is my personal blog as well as the pet project for learning SSG and DevOps practices such as:

* [Ansible](https://docs.ansible.com/)
* [Docker](https://docs.docker.com/get-started/overview/) и [Docker Compose](https://github.com/compose-spec/compose-spec/blob/master/spec.md)
* [Gitlab CI](https://docs.gitlab.com/ee/)
* [Nginx](https://nginx.org/en/docs/)
* [HUGO](https://gohugo.io/documentation/)
* to be continued...

Project currently __is under construction__... Source code of this project you may find in self-hosted repository by link in menu above or by link to Gitlab repo on main page.
