---
title: "Контакты"
date: 2022-05-24T14:28:06Z
draft: false
---
## Моё резюме:
* [На русском](https://resume.io/r/oJOXZa0SN) — [Google Docs](https://docs.google.com/document/d/1WuXWTaU94D-pVNFMFVoPZsYnRIzZoWUez47-UtXUZUo/edit?usp=sharing)
* [На английском](https://resume.io/r/FlTVHfKWY) — [Google Docs](https://docs.google.com/document/d/1Kg8rLEqDWS01FUVlrsQc4qbg0tZ57iTOQz08AVzz0rk/edit?usp=sharing)
## Ссылки на мои аккаунты:
* [LinkedIn](https://www.linkedin.com/in/sergei-valuev/)
* [Twitter](https://twitter.com/ssvaluev/)
* [Gitlab](https://gitlab.com/valuevs/)
* [Telegram](https://t.me/feeler): @feeler
* [Skype](https://join.skype.com/invite/omt8t9GI3Viq): @valuevss
