+++
title = "О сайте"
description = "Что это за сайт и кто я такой"
date = "2022-05-24"
aliases = ["about-us", "about-hugo", "contact"]
author = "Сергей Валуев"
+++

Привет! Меня зовут Валуев Сергей. Я DevOps инжинер, в недавнем прошлом системный  администратор.

Этот сайт является моим персональным блогом, а также учебным проектом для изучения SSG и инструментов практик методологии DevOps таких как:

* [Ansible](https://docs.ansible.com/)
* [Docker](https://docs.docker.com/get-started/overview/) и [Docker Compose](https://github.com/compose-spec/compose-spec/blob/master/spec.md)
* [Gitlab CI](https://docs.gitlab.com/ee/)
* [Nginx](https://nginx.org/ru/docs/)
* [HUGO](https://gohugo.io/documentation/)
* продолжение следует...

__Проект находится в разработке__... Код данного проекта можно найти в self-hosted репозитории по ссылке в меню или в публичном репозитории Gitlab на главной странице.